for name in ["Howard", "Dave", "Joe"]:
    if name == "Dave":
        print(f"Hello {name}")
        continue
    elif name == "Joe":
        print(f"Good Morning {name}")
        continue
    print(f"Good Evening {name}")

format_string = "Price: £{}"

print(format_string.format(80))